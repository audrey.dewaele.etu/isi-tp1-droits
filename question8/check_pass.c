#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "check_pass.h"

int main(int argc, char *argv[])
{
	return 0;
}

bool check_pw(char *username, char *password) //return if the password given is the password registered with the username given
{
	if(exist(username))
	{
		char* recorded_pw = get_back(username);
		if(strcmp(recorded_pw,password) == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		printf("user not referenced\n");
		return false;
	}
}

char* get_back(char *username) //return the password associated to the username given
{
	char *user;
	char *password;
	char *line;
	FILE *f = fopen("/home/admin/passwd.txt","r");
	while(line = fgetc(f) != EOF)
	{
		user = strtok(line, " ");
		password = strtok(line, " ");

		if(strcmp(user,username) == 0)
		{
			fclose(f);
			return password;
		}
	}
	fclose(f);
	return password;
}

bool exist(char *username)
{
	FILE *f = fopen("/home/admin/passwd.txt","r");
	char *ptr;
	int line;

	line =fgetc(f);
	while((line = fgetc(f)) != EOF)
	{
		ptr = strtok(line, " ");
		if(strcmp(ptr,username) == 0)
		{
			fclose(f);
			return true;
		}
		ptr = strtok(line, " ");
	}
	fclose(f);
	return false;
}