#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "check_pass.h"
#include "check_group.h"
#include "getuser.h"

int main(int argc, char *argv[])
{
	char *pw;
	char *username;
	char *file = argv[0];

	printf("Enter the password:\n");
	scanf("%s",pw);

	username = getUser();
	if(check_pw(username,pw))
	{
		if(check_gp(username,file))
		{
			return(remove(file));
		}
		else
		{
			printf("user doesn't belong to the same group as the file's group owner\n");
		}
	}
	return false;
}