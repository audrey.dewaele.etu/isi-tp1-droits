#créer 2 groupes
addgroup group_a
addgroup group_b

#créer 3 utilisateurs
adduser lambda_a group_a
adduser lambda_b group_b
adduser administrateur

#créer 3 dossiers
mkdir dir_a
mkdir dir_b
mkdir dir_c

#créer 3 fichiers dans chacun des dossiers
touch dir_a/file_a
touch dir_b/file_b
touch dir_c/file_c

#mise à jour des permissions
chmod 770 dir_a #rwx rwx ---
chmod 770 dir_b #rwx rwx ---
chmod 775 dir_c #rwx rwx r-x
chmod 750 dir_a/file_a #rwx r-x ---
chmod 750 dir_b/file_b #rwx r-x ---
chmod 775 dir_c/file_c #rwx rwx r-x
chmod +t dir_a # met comme propriétaire des fichiers de dir_a le même propriétaire que le propriétaire du dossier 
chmod +t dir_b 
chmod +t dir_c

chown administrateur:group_a dir_a # Changement de l'utilisateur et du groupe propriétaire d'ubuntu à administrateur et group_a.
chown administrateur:group_b dir_b
chown administrateur:administrateur dir_c
