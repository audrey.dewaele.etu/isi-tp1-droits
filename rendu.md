# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Clopier-Duquenne, Mélanie, melanie.clopierduquenne.etu@univ-lille.fr

- Nom, Prénom, email: Dewaele, Audrey, audrey.dewaele.etu@univ-lille.fr

## Question 1

L'utilisateur "toto" peut écrire dans le fichier titi.txt car bien que l'écriture ne soit pas permise à tous les utilisateurs, le groupe ubuntu détient le droit en écriture et "toto" faisant partie du groupe ubuntu, il hérite donc de ces droits.

## Question 2

Le caractère x pour un répertoire signifie le droit d'ouvrir le répertoire.
Lorsque "toto" tente d'entrer dans le répertoire après que les droits aient été changés, la permission d'y entrer est refusée. Celle-ci est refusée car le groupe dont "toto" fait partie n'a pas le droit d'exécuter le dossier.
Lorsque "toto" fait ls -al mydir, on voit la liste des fichiers se trouvant dans mydir tout en se voyant préciser que l'accès nous est refusé et nous ne pouvons pas non plus voir les permissions de tout fichier se trouvant dans le dossier.

## Question 3

La valeur des ids est 1001 et le processus ne parvient pas à ouvrir le fichier.
Lorsque nous activons le flag set-user-id, les ids GID, RUID et RGID ont la valeur 1001 tandis que l'UID est à 1000. De plus, le processus parvient à ouvrir le fichier et le lire.


## Question 4

Les ids ont tous la valeur 1001 qui est la valeur de l'id de "toto".

## Question 5

chfn permet de modifier les données personnelles des utilisateurs contenues dans /etc/passwd.
Lorsqu'un utilisateur tape 
```bash
ls -al /usr/bin/chfn
```
le résultat est : 
```bash
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
```

Ces permissions indiquent que root a accès au fichier en lecture, écriture et que le groupe et les autres utilisateurs ont accès uniquement à la lecture et à l'exécution. Lorsque "toto" lance la commande chfn, il peut modifier ses informations personnelles et ces modifications sont répercutées dans le fichier /etc/passwd.

## Question 6

Les mots de passe sont stockés dans le fichier /etc/shadow. Ce fichier n'est accessible qu'à root ce qui limite les risques de vol de mot de passe. De plus, les mots de passe ne sont pas stockés en clair, il est donc difficile de les connaître.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.
Notre utilisateur admin s'appelle administrateur car comme le groupe admin existe déjà, nous aurions dû créer un groupe ayant un nom différent de celui de notre utilisateur.

Il y a 3 utilisateurs : Lambda_a qui appartient à groupe_a, Lambda_b qui appartient à groupe_b et administrateur qui appartient à groupe_a et groupe_b et au groupe « administrateur ».
Soient les droits suivants :
*	D1 = droit de lecture d’un fichier ou du contenu d’un dossier.
*	D2 = droit de modifier le fichier ou le contenu d’un dossier et créer un nouveau fichier dans un dossier.
*	D3 = droit d’effacer ou de renommer.
*	D3P = D3 uniquement pour le propriétaire 
*	~D1, ~D2 et ~D3 les contraires de respectivement D1, D2 et D3.

L’objectif est le suivant :

|    | Propriétaire | Groupe propriétaire | Lambda_a | Lambda_b | Administrateur |
|:--:|:------------:|:-------------------:|:--------:|:--------:|:-------------:|
|Dir_a/File_a| Lambda_a | Groupe_a | D1 D2 D3P | ~D1 ~D2 ~D3 | D1 ~D2 D3 |
| Dir_b/File_b | Lambda_b | Groupe_b | ~D1 ~D2 ~D3 | D1 D2 D3P | D1 ~D2 D3 |
| Dir_c/File_c | Administrateur | Administrateur | D1 ~D2 ~D3 | D1 ~D2 ~D3 | D1 D2 D3 |
| Dir_a/dir_aBIS | Lambda_a | Groupe_a | D1 D2 D3 | ~D1 ~D2 ~D3 | D1 ~D2 D3 |
| Dir_b/dir_bBIS | Lambda_b | Groupe_b | ~D1 ~D2 ~D3 | D1 D2 D3 | D1 ~D2 D3 |
| Dir_c/dir_cBIS | Administrateur | Administrateur | D1 ~D2 ~D3 | D1 ~D2 ~D3 | D1 D2 D3 |

Ce qui donne :
* R = lecture
* W = écriture
* X = exécution

|    | File_a | File_b | File_c | Dir_a | Dir_b | Dir_c |
|:--:|:------:|:------:|:------:|:-----:|:-----:|:-----:|
| Propriétaire | RWX | RWX | RWX | RWX | RWX | RWX |
| Groupe | R-X | R-X | RWX | RWX | RWX | RWX | RWX |
| Autre | --- | --- | R-X | --- | --- | R-X |

Enfin, si on a :
* R=4
* W=2
* X=1
* -=0

En additionnant ces valeurs, on obtient pour chaque case :

|    | File_a | File_b | File_c | Dir_a | Dir_b | Dir_c |
|:--:|:------:|:------:|:------:|:-----:|:-----:|:-----:|
| Propriétaire | 7 | 7 | 7 | 7 | 7 | 7 |
| Groupe | 5 | 5 | 7 | 7 | 7 | 7 | 7 |
| Autre | 0 | 0 | 5 | 0 | 0 | 5 |

Que l’on peut appliquer à la commande CHMOD dans l’ordre suivant : Propriétaire + Groupe + Autre.

Mettre un sticky-bit sur Dir_a et Dir_b permet de respecter le droit D3P cité précédemment. 


## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








