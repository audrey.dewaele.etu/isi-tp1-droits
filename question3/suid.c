#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[]) {

        uid_t uid = geteuid();
        gid_t gid = getegid();
        uid_t ruid = getuid();
        gid_t rgid = getgid();

        printf("Voici l'uid:: %ld\n",(long int) uid);
        printf("Voici le gid:: %ld\n",(long int) gid);
        printf("Voici le ruid:: %ld\n",(long int) ruid);
        printf("Voici le rgid:: %ld\n",(long int) rgid);

        FILE *f;

        if(argc < 2) {
                printf("Missing argument\n");
                exit(EXIT_FAILURE);
        }
        f = fopen(argv[1], "r");
        int actual = 0;
        while((actual=fgetc(f))!=EOF)
        {
            printf("%c", actual);
        }
        printf("\n");
        if(f==NULL) {
                perror("Cannot open file");
                exit(EXIT_FAILURE);
        }
        printf("File opens correctly\n");
        fclose(f);
        return 0;
}


