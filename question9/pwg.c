#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "getuser.h"
#include "account.h"

bool main()
{
	bool result;
	char *pwEnrolled;
	char *pw;
	char *user = getUser();

	if(isEnrolled(user))
	{
		printf("You are already enrolled. Enter your former password to change it\n");
		scanf("%s",pw);
		pwEnrolled = recoverPassword(user);
		if(strcomp(pwEnrolled,pw) == 0)
		{
			result = newPassword(user);
			return result;
		}
		else
		{
			printf("Your former password is wrong\n");
		}
	}
	else
	{
		result = newPassword(user);
		return result;
	}

	return false;
}